extends Node
class_name Game

signal dialogue_begin()
signal dialogue_next()
signal dialogue_end()

onready var gui: GUI = $CanvasLayer/GUI

var in_dialogue_with: Node = null

# Called when the node enters the scene tree for the first time.
func _ready():	
	update_debug_gui()
	connect_signals()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func update_debug_gui():
	var current_level = $Level.get_filename()
	gui.set_debug_text("Level: %s" % current_level)
	
func connect_signals():
	var err = $Level/YSort/PlayerCharacter.connect("initiate_dialogue", self, "_on_PlayerCharacter_initiate_dialogue")
	if err:
		push_error(err)

func _on_Level_ready():
	yield(get_tree(), "idle_frame")
	update_debug_gui()
	connect_signals()

func change_level(num: int):
	var new_scene = "res://scenes/Level%s.tscn" % num
	
	var cur_level = $Level
	self.remove_child(cur_level)
	cur_level.call_deferred("free")

	var new_level_res = load(new_scene)
	var new_level: Node = new_level_res.instance()

	print("Changed level to " + new_scene)
	var err = new_level.connect("ready", self, "_on_Level_ready")
	if err:
		push_error(err)

	self.add_child(new_level)
	new_level.set_name("Level")

func _on_PlayerCharacter_initiate_dialogue(other: Node):
	in_dialogue_with = other
	var dialogues: Dictionary = other.get_dialogue()
	$CanvasLayer/GUI/BottomRow/DialogueBox.set_visible(true)
	emit_signal("dialogue_begin")

	var id = "1"
	while id != null:
		var d: Dictionary = dialogues[String(id)]
		var name = d["name"]
		var body = d["body"]
		var options = d.get("options", [[">>", null]])
		$CanvasLayer/GUI/BottomRow/DialogueBox.set_dialogue(name, body, options)
		print("[%s] \"%s\"" % [name, body])
		var next_target = yield($CanvasLayer/GUI, "dialogue_next")
		id = next_target
		emit_signal("dialogue_next")

	in_dialogue_with = null
	$CanvasLayer/GUI/BottomRow/DialogueBox.set_visible(false)
	$CanvasLayer/GUI/BottomRow/DialogueBox.free_buttons()
	emit_signal("dialogue_end")
	
