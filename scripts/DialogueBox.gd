extends ColorRect
class_name DialogueBox

signal option_pressed(text, target)

var btn_res = preload("res://scenes/DialogueOptionButton.tscn")

func set_dialogue(name: String, body: String, options: Array):
	$Name.set_text(name)
	$Body.set_text(body)
	
	for child in $HBoxContainer.get_children():
		$HBoxContainer.remove_child(child)

	for option in options:
		var opt_txt = option[0]
		var opt_target = option[1]

		var btn: Button = btn_res.instance()
		btn.set_text(opt_txt)
		var err = btn.connect("pressed", self, "_on_DialogueOptionButton_pressed", [opt_txt, opt_target])
		if err:
			return push_error(err)
		$HBoxContainer.add_child(btn)
	
func free_buttons():
	$HBoxContainer.call_deferred("queue_free")

func _on_DialogueOptionButton_pressed(text: String, target):
	emit_signal("option_pressed", text, target)
