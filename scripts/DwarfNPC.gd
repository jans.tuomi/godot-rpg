extends KinematicBody2D
class_name DwarfNPC

export (NodePath) var target
export (String, FILE, "*.json") var dialogue_file
onready var target_node = get_node(target)
var dialogue_data = {}

func load_text_file(path):
	var f = File.new()
	var err = f.open(path, File.READ)
	if err != OK:
		printerr("Could not open file, error code ", err)
		return ""
	var text = f.get_as_text()
	f.close()
	return text

# Called when the node enters the scene tree for the first time.
func _ready():
	var dialogue_txt = load_text_file(dialogue_file)
	var dialogue_result = JSON.parse(dialogue_txt)
	if dialogue_result.error != OK:
		return push_error(dialogue_result.error_string)
	dialogue_data = dialogue_result.result

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	# Look at target node
	var target_x: float = target_node.position[0]
	var dx = target_x - self.position[0]
	var dir = -1 if dx < .0 else 1
	$Sprite.set_scale(Vector2(dir, 1))

func get_dialogue():
	return dialogue_data
