extends MarginContainer
class_name GUI
export (bool) var show_debug = true
onready var debug_label = $TopRow/DebugLabel

signal dialogue_next(target)

# Called when the node enters the scene tree for the first time.
func _ready():
	debug_label.set_visible(show_debug)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func _unhandled_input(event: InputEvent):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ESCAPE:
			get_tree().quit()
#		if event.pressed and event.is_action_pressed("ui_accept"):
#			emit_signal("dialogue_next")

func set_debug_text(text: String):
	debug_label.set_text(text)

func _on_DialogueBox_option_pressed(_text, target):
	emit_signal("dialogue_next", target)
